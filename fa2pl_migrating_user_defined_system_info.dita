<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="savfcb" xml:lang="en-us">
<title>Migrating User-Defined System Information</title>
<prolog>
<metadata>
<keywords><!--Insert &lt;indexterm&gt; tags here--></keywords></metadata></prolog>
<conbody>
<p>The system sublibrary (IJSYSRS.SYSLIB) of your <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> system may include user-defined system information.
Examples of such information are: <ul rev="r62">
<li>FCB- and UCB-image phases of your own stored by the <tm trademark="IBM" tmtype="reg">IBM</tm>
standard names.</li>
<li>Label information.</li>
<li>Private SVA load lists.</li>
<li>Modified IPL and JCL procedures.</li></ul></p>
<p>Any such information is lost if you simply perform <b>initial installation</b>. To
			avoid this, proceed as outlined below.</p>
<p rev="r62">Ensure that IBM supplied specifications relating to the new release (if any) will be
included in the procedures that are to be migrated.</p>
<note>Numbers within parentheses in front of individual statements refer to additional
			information under <xref href="fa2pl_explanations_sample_jobs.dita#jobnts"></xref>.</note>
<ol>
<li><b>Produce a backup copy of sublibrary PRD2.SAVE</b>
				<p>This step assumes that you had defined and processed your FCB- and UCB-image
					phases, using the <keyword conref="fa299ent.dita#common-fa299sym/ii"></keyword> of <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> as described in <ph otherprops="toPDF" rev="JF">the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/admnx"></keyword></ph><xref href="fa2ad_xref_target_for_toc.dita" format="dita" otherprops="toKC" rev="JF"></xref>. In that case, these phases were saved in the sublibrary
					PRD2.SAVE of your system. Perform this step on your current VSE system.</p>
				<p>To produce the backup, either use the <i>Backup/Restore Library Objects</i>
					dialog with <i>Backup VSE Library on Tape</i> or run a job similar to the
					sample below.
					<codeblock>        // JOB BACKUP PRD2.SAVE SUBLIBRARY
        // MTC  REW,cuu
        // EXEC LIBR
    (1) BACKUP S=PRD2.SAVE TAPE=cuu
        /*
        /&amp;</codeblock></p>
				<p>This step saves all control information stored in your PRD2.SAVE library.</p>
				<p>If, because of a locally defined procedure, this control information is not
					stored in the sublibrary PRD2.SAVE, produce a backup of the system
					sublibrary. The corresponding BACKUP statement would be:
					<codeblock>    BACKUP S=IJSYSRS.SYSLIB TAPE=cuu</codeblock></p>
				<p>As concerns label information stored on the system residence volume, ensure
					that your original load procedures are stored in a sublibrary on a private
					volume.</p></li>
<li><b>Edit your JCL ASI and label procedures</b>
				<p>Do this after having performed the first system startup with the newly
					installed refresh release. This is a multi-step process as follows: <ol>
<li>Restore the sublibrary PRD2.SAVE into a separate sublibrary.
							Either use the <i>Backup/Restore Library Objects</i> dialog with
								<i>Restore VSE Library from Tape</i> or run a job similar to
							the sample below. <codeblock>        // JOB  RESTORE PRD2.SAVE
        // MTC  REW,cuu
        // EXEC LIBR,PARM='MSHP'
    (2) RESTORE S=PRD2.SAVE:PRD2.SAVEOLD TAPE=cuu DATE=OLD
        /*
        /&amp;</codeblock>
							<p>If there was a need to backup your system sublibrary, replace
								the librarian RESTORE command by a number of selective
								restore requests. Each of your restore requests would have
								to specify the generic name of the procedures that are to be
								restored. The librarian RESTORE command is described in <ph otherprops="toPDF" rev="JF">your <xref keyref="scsz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/scs"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/scsx"></keyword>
									under <q>RESTORE</q></ph><xref href="fa2sc_r_librarian_restore.dita" otherprops="toKC" rev="JF"></xref>.</p></li>
<li>Select VSE/ICCF command mode.</li>
<li>Punch, into your primary VSE/ICCF library, the startup procedures
							that you want to migrate. To do this, use the LIBRP command as
							shown: <codeblock>    LIBRP PRD2.SAVEOLD vsemembername.PROC iccfmembername</codeblock>
							<p>If you have used the default names for your <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> startup
								procedures, you may have to issue such a request for each of
								the following procedures: <dl>
<dlentry>
<dt><codeph>ALLOC</codeph></dt>
<dd>If you changed the partition allocations.</dd></dlentry>
<dlentry>
<dt><codeph>$0JCL</codeph></dt>
<dd>If you changed the startup procedure for
										BG.</dd></dlentry>
<dlentry>
<dt><codeph>$1JCL</codeph></dt>
<dd>If you changed the startup procedure for the
										VSE/POWER partition.</dd></dlentry>
<dlentry>
<dt><codeph>POWSTRT</codeph></dt>
<dd>If you changed the VSE/POWER autostart
										procedure.</dd></dlentry>
<dlentry>
<dt>Any of <codeph>$2JCL</codeph> through
										<codeph>$BJCL</codeph></dt>
<dd>If you changed the startup procedures for
										partition F2 through FB.</dd></dlentry>
<dlentry>
<dt><codeph>STDLABEL</codeph> and
										<codeph>STDLABUP</codeph></dt>
<dd>Get the new label procedures and edit them to
										get your own label definitions into the
										procedures.</dd></dlentry></dl></p>
							<p>Use the corresponding names of your own choice if you have not
								used the <tm trademark="IBM" tmtype="reg">IBM</tm> standard names.</p></li>
<li>Ensure that <tm trademark="IBM" tmtype="reg">IBM</tm> supplied specifications relating to
							new support (if any) will be included in the procedures that are
							to be migrated.</li>
<li>Verify your changes – on a test system, if possible.
								<p>Perform step <xref href="#savfcb/stepn"></xref> on a test system rather than on
								your system for normal operation and subsequently start up
								the test system.</p></li>
<li>Return the edited procedures to the sublibrary from which they
							were punched into your VSE/ICCF library. <p>Use LIBRC macro
								requests to accomplish this. The macro works in a way
								similar to the LIBRP macro but in reverse
							direction.</p></li></ol></p></li>
<li id="stepn"><b>Restore the procedures to the system sublibrary</b>
				<p>Copy the edited procedures to the system sublibrary. Use a job similar to the
					one shown below:
					<codeblock>        // JOB COPY PROCEDURES
        // EXEC LIBR,PARM='MSHP'
        CONNECT S=PRD2.SAVEOLD:IJSYSRS.SYSLIB
    (3) COPY vsemembername.PROC REPLACE=YES DATE=OLD
        /*
        /&amp;</codeblock></p></li>
<li><b>Restore the FCB and UCB-image phases</b>
				<p>If you use the interactive interface, do an administrator fast path selection
					of <b>3722</b> and provide the required control information.</p>
				<p>If you do not use the interactive interface, run a job similar to the one
					below:
					<codeblock>        // JOB RESTORE FCB PHASES
        // MTC  REW,cuu
        // EXEC LIBR,PARM='MSHP'
    (4) RESTORE PRD2.SAVE.$$BFCB*.PHASE : IJSYSRS.SYSLIB -
                TAPE=cuu REPLACE=YES DATE=OLD
    (5) RESTORE PRD2.SAVE.$$BUCB*.PHASE : IJSYSRS.SYSLIB -
                TAPE=cuu REPLACE=YES DATE=OLD
        /*
        /&amp;</codeblock></p></li>
<li><b>Make the changes effective</b>
				<p>IPL the modified system to make the changes effective.</p></li></ol></conbody><?tm 1447078443 3?></concept>