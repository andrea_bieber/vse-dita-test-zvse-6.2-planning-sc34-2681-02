<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="tapes" xml:lang="en-us">
<title>Tape Device Support</title>
<prolog>
<metadata>
<keywords><!--Insert &lt;indexterm&gt; tags here--></keywords></metadata></prolog>
<conbody>
<p>This topic describes the <i>tape devices</i> that are supported by <keyword conref="fa299ent.dita#common-fa299sym/vcurrent"></keyword>.</p>
<p otherprops="pdf toPDF">It contains these sub-topics: <ul>
<li><xref href="fa2pl_ovw_tape_driveslibs_supported_zvse.dita#ovwtapes"></xref></li>
<li><xref href="fa2pl_ibm_ts1130_ts1120_3592_j1a_tape_drives.dita#d1130"></xref></li>
<li><xref href="fa2pl_ts3400_autoloader_tape_lib.dita#ts3400"></xref></li>
<li><xref href="fa2pl_tape_drives.dita#x590"></xref></li>
<li><xref href="fa2pl_ibm_tape_drive.dita#plg54"></xref></li>
<li><xref href="fa2pl_ibm_ts7680_gateway_system_z.dita#ts7680"></xref></li>
<li><xref href="fa2pl_ibm_ts7700_virtualization_engine.dita#ts7700"></xref></li>
<li><xref href="fa2pl_ibm_ts35003584_ultrascalable_tape_lib_.dita#tapl3584"></xref></li>
<li><xref href="fa2pl_ibm_tape_lib.dita#tl44"></xref></li>
<li><xref href="fa2pl_ibm_vts_virtual_tape_server.dita#vts3494"></xref></li>
<li><xref href="fa2pl_how_zvse_supports_tape_libs.dita#tlsuse"></xref></li></ul></p>
<p otherprops="toPDF"><b>Related Topics:</b>
			<table pgwide="0" rowheader="firstcol">
<tgroup cols="2"><colspec colname="col1" colwidth="116*"></colspec><colspec colname="col2" colwidth="81*"></colspec>
<thead>
<row valign="bottom">
<entry colname="col1">For details of how to …</entry>
<entry colname="col2">Refer to …</entry></row></thead>
<tbody>
<row>
<entry colname="col1">use the <i>Interactive Interface</i> to add a
								tape drive, including specifying the tape mode (encrypted,
								buffered write, compacted)</entry>
<entry colname="col2"><q><keyword conref="fa299ent.dita#common-fa299sym/hdadmds"></keyword></q>
								in <ph otherprops="toPDF" rev="JF">the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref></ph><xref href="fa2ad_xref_target_for_toc.dita" format="dita" otherprops="toKC" rev="JF"></xref>.</entry></row>
<row>
<entry colname="col1">configure a tape library for use with z/VSE's
									<i>Tape Library Support</i> (TLS)</entry>
<entry colname="col2"><q><keyword conref="fa299ent.dita#common-fa299sym/hdadmtl"></keyword></q>
								in the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>.</entry></row>
<row>
<entry colname="col1">								<ul compact="yes">
<li>add a tape drive to the IPL startup using the ADD
										statement</li>
<li>assign a tape drive to a system logical unit using
										the ASSGN statement</li>
<li>specify the tape mode (encrypted, buffered write,
										compacted, and so on) using the ASSGN
										statement</li>
<li>perform tape library functions (for example issue a
										LIBSERV MOUNT or LIBSERV EJECT command)</li></ul></entry>
<entry colname="col2"><xref keyref="scsz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/scs"></keyword></i></xref>.</entry></row>
<row>
<entry colname="col1">encrypt tapes using encryption-capable tape
								drives</entry>
<entry colname="col2"><q>Configuring for Hardware-Based Tape
									Encryption</q> in the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>.</entry></row>
<row>
<entry colname="col1">backup/export to encrypted tapes using the
								Interactive Interface</entry>
<entry colname="col2"><ph otherprops="toPDF" rev="JF"><q>Backing Up and Restoring Data</q> in the <xref keyref="operz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/oper"></keyword></i></xref></ph><xref href="fa2op_back_up_restore_data.dita" otherprops="toKC" rev="JF"></xref>. </entry></row>
<row>
<entry colname="col1">setup and configure the TS3400
								autoloader</entry>
<entry colname="col2">technical article <q>Overview of <tm trademark="IBM" tmtype="reg">IBM</tm>
									<tm trademark="System Storage" tmtype="reg">System Storage</tm>
									TS1120 Tape Controller Support for <tm trademark="System Storage" tmtype="reg">System Storage</tm>
									TS3400 Tape Library</q>, which you can find in the
									<q>Documentation</q> section of the <i><keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>
									Homepage</i> (whose URL is given in <xref href="fa2pl_where_find_more_info.dita#plg3"></xref>).</entry></row></tbody></tgroup></table></p></conbody><?tm 1447078417 3?></concept>