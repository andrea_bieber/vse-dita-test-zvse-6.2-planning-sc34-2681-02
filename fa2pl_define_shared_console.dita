<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="con2" xml:lang="en-us">
<title>Defining a Shared Console</title>
<prolog>
<metadata>
<keywords><indexterm>shared<indexterm>console under VM</indexterm></indexterm></keywords></metadata></prolog>
<conbody>
<p>Instead of defining a dedicated console for <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>, you can operate
the system from a <i>shared</i> console.  In this environment, the <i>same</i> VM
terminal used to log on and IPL the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> virtual machine
is also used as the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> console. </p>
<p>To use a shared console:  <ol>
<li>From a VM terminal, log on using the ID and password defined for
the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> virtual
machine.</li>
<li>Enter the following CP commands <i>after</i> you IPL CMS but <i>before</i> you
IPL <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>:
 <codeblock><b>CP SP RDR CONT
CP SET RUN ON
CP TERM CONMODE 3270
CP TERM BREAKIN GUEST</b></codeblock> <p>Besides manually entering
these commands in CP mode, you can also make them a part of a PROFILE
EXEC that you define for the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> virtual machine. <xref href="fa2pl_define_cms_profile_exec.dita#addo"></xref> shows how to
do this.</p></li>
<li>IPL <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>.
 The <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> console
screen will be displayed.</li>
<li>If you want to return to VM from the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> console, press
the <b>PA1</b> key.  This puts the screen in CP mode.</li>
<li>To return to the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> console
from CP mode, enter the command <b>B</b> (for <b>CP BEGIN</b>).</li>
<li>When you are done using the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> console, enter
the following command in the command line at the bottom of the console
screen:  <codeblock><b>* CP DISC</b></codeblock></li></ol> </p></conbody><?tm 1447078453 0?></concept>