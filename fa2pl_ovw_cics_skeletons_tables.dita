<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="cst24" xml:lang="en-us">
<title>Overview on <tm trademark="CICS" tmtype="reg">CICS</tm> Skeletons and Tables</title>
<prolog>
<metadata>
<keywords><indexterm>CICS skeletons and tables</indexterm><indexterm>CICS Transaction Server<indexterm>skeletons and
					tables</indexterm></indexterm><indexterm>CICS Transaction Server<indexterm>autoinstall
					exit</indexterm></indexterm></keywords></metadata></prolog>
<conbody>
<p><xref href="#cst24/cc24"></xref><ph> shows for the predefined <tm trademark="CICS" tmtype="reg">CICS</tm> systems the available skeletons and tables. The
				figure allows you to identify quickly the differences and the requirements for
				the:</ph>
			<ul>
<li>Primary <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword> which is available
					after initial installation and which is running in partition F2.</li>
<li>Secondary <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword> which must be
					installed separately (default partition F8) as described under <xref href="fa2pl_installing_2nd_cics_transaction_server.dita#iccts"></xref>.</li></ul>
			<ph>The source code of all the skeletons, tables, and members provided is available
				in <b><keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword> library 59</b>.</ph></p>
<table frame="all" pgwide="0" rowheader="firstcol" id="cc24">
<title><tm trademark="CICS" tmtype="reg">CICS</tm> Skeletons and Tables</title>
<tgroup cols="3" colsep="1" rowsep="1"><colspec colname="col1" colwidth="2*"></colspec><colspec colname="col2" colwidth="1*"></colspec><colspec colname="col3" colwidth="1*"></colspec>
<thead>
<row valign="bottom">
<entry rowsep="1"> Skeletons/Tables</entry>
<entry rowsep="1">Primary <tm trademark="CICS" tmtype="reg">CICS</tm> TS (DBDCCICS)</entry>
<entry rowsep="1">Secondary <tm trademark="CICS" tmtype="reg">CICS</tm> TS (PRODCICS)</entry></row></thead>
<tbody>
<row>
<entry>Startup Skeleton</entry>
<entry>SKCICS</entry>
<entry>SKCICS2</entry></row>
<row>
<entry>Resource Definition Skeleton</entry>
<entry>—</entry>
<entry>SKPREPC2</entry></row>
<row>
<entry>Destination Control Table</entry>
<entry>DFHDCTSP</entry>
<entry>DFHDCTC2</entry></row>
<row>
<entry>Monitoring/Statistics Table</entry>
<entry>DFHDMFSP</entry>
<entry>—</entry></row>
<row>
<entry>File Control Table</entry>
<entry>DFHFCTSP *</entry>
<entry>DFHFCTC2 *</entry></row>
<row>
<entry>Journal Control Table</entry>
<entry>DFHJCTSP</entry>
<entry>—</entry></row>
<row>
<entry>Processing Program Table</entry>
<entry>—</entry>
<entry>—</entry></row>
<row>
<entry>Program Control Table</entry>
<entry>—</entry>
<entry>—</entry></row>
<row>
<entry>Program List Table (startup)</entry>
<entry>DFHPLTPI</entry>
<entry>DFHPLTP2</entry></row>
<row>
<entry>Program List Table (shutdown)</entry>
<entry>DFHPLTSD (see Note)</entry>
<entry>DFHPLTS2</entry></row>
<row>
<entry>System Initialization Table</entry>
<entry>DFHSITSP</entry>
<entry>DFHSITC2</entry></row>
<row>
<entry>Temporary Storage Table</entry>
<entry>DFHTSTSP</entry>
<entry>—</entry></row>
<row>
<entry>Terminal Control Table</entry>
<entry>—</entry>
<entry>—</entry></row>
<row>
<entry>Transaction List Table</entry>
<entry>DFHXLTSP</entry>
<entry>DFHXLTSP</entry></row>
<row>
<entry>Sign-on Table</entry>
<entry>—</entry>
<entry>—</entry></row></tbody></tgroup></table>
<p>(*) These tables are migrated into the CSD during initial installation or FSU.</p>
<note>DFHPLTSD includes a minimum support for statistics to be printed at shutdown. This
			support is invoked if DFH0STAT has been activated in DFHPLTSD.</note>
<p>The following source books for CSD definitions are available in IJSYSRS.SYSLIB:</p>
<codeblock>IESWPPT.Z     Program definitions for workstations
IESWPPTL.Z    Program definitions for workstations (Multicultural Support)
IESZPPT.Z     Program definitions for Interactive Interface
IESZPPTI.Z    Program definitions for Interactive Interface-VSE/ICCF
IESZPPTL.Z    Program definitions for Interactive Interface-VSE/ICCF 
              (Multicultural Support)
IESWPCT.Z     Transaction definitions for workstations
IESZPCT.Z     Transaction definitions for Interactive Interface
IESZFCT.A     File definitions
IESZTCT.Z     Terminal and console definitions
IESCSEZA.Z    CICS Listener definitions
CEECCSD.Z     LE/VSE Base definitions
IBMCCSD.Z     PL/I Run-Time definitions
IGZCCSD.Z     COBOL Run-Time definitions
EDCCCSD.Z     C Run-Time definitions  </codeblock>
<p>The following source books for CSD definitions are available in PRD2.SCEEBASE:</p>
<codeblock>CEETICSD.Z    Set USESVACOPY(YES) for LE/VSE Base
EDCTICSD.Z    Set USESVACOPY(YES) for C Run-Time</codeblock>
<p>The following source book for CSD definitions is available in PRD2.TCPIPC:</p>
<codeblock>IPNCSD.Z      TCP/IP definitions </codeblock>
<p><b>Autoinstall Exit</b></p>
<p>Autoinstall exits are used for <tm trademark="VTAM" tmtype="reg">VTAM</tm> terminal definitions according to predefined
			models.</p>
<p>The autoinstall exit member for the <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword> is IESZATDX. This exit member is
			available as a skeleton of the same name in <keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword> library 59.</p></conbody><?tm 1447078472 6?></concept>