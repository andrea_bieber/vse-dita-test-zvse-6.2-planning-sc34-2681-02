<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="imhint" xml:lang="en-us">
<title>Overview of Security-Support Implementation</title>
<prolog>
<metadata>
<keywords><indexterm>security support<indexterm>implementing
					security</indexterm></indexterm><indexterm>security support<indexterm>security server
					partition</indexterm></indexterm><indexterm>security server partition</indexterm></keywords></metadata></prolog>
<conbody>
<p>			<ul>
<li>The IPL SYS command contains the following security parameters. <codeblock>SEC=NO
SEC=YES
SEC=(YES,JCL)
SEC=(YES,NOTAPE)
SEC=(YES,NOTAPE,JCL)
SEC=RECOVER
ESM=name
SERVPART=partition</codeblock>
					<ul compact="yes">
<li>These parameters are modified using the <i>Tailor IPL
								Procedure</i> dialog.</li>
<li>For a detailed description of the IPL SYS command, refer to <ph otherprops="toPDF" rev="JF">the <xref keyref="scsz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/scs"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/scsx"></keyword></ph><xref href="fa2sc_xref_target_for_toc.dita" format="dita" otherprops="toKC" rev="JF"></xref>.</li></ul></li>
<li>Initial Installation <p>During initial installation you are asked whether
						you want to run your system with "security on". <ul>
<li>If you respond with YES, this will set SEC=(YES,NOTAPE) in
								the IPL SYS command. This also provides access control for
								resources defined in DTSECTAB.</li>
<li>If you respond with NO during initial installation and want
								to activate security later, you can later use the <i>Tailor
									IPL Procedure</i> dialog to change SEC=NO into SEC=YES,
								SEC=(YES,JCL), SEC=(YES,NOTAPE), or SEC=(YES,NOTAPE,JCL).
								However, security activation also requires additional <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>
								modifications to be done manually. To avoid this effort and
								if you definitely plan to implement security later, it is
								recommended to proceed as follows: <ul compact="yes">
<li>Respond with YES to the security question during
										initial installation. This prepares and
										initializes the system for security activation and
										avoids later customization work.</li>
<li>You can now use the IPL SYS command and specify
										SEC=NO if you want to run your system without
										table DTSECTAB security for the time being
										(DTSECTAB includes resource definitions only,
										except for the predefined users DUMMY and FORSEC).
										With SEC=YES you can at any time switch security
										on. No further customization effort is required
										except including your own entries in
										DTSECTAB.</li></ul></li></ul></p>
					<p>If you use the option JCL, the system will perform JCL security checking
						using <i>JCL resource profiles</i>. For details, see <ph otherprops="toPDF" rev="JF"><q rev="JF">Protecting Resources via BSM Dialogs</q> in the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/admnx"></keyword></ph><xref href="fa2ad_protecting_resources_via_bsm_dialogs.dita" otherprops="toKC" rev="JF"></xref>.</p></li>
<li>Security Server Partition <p>BSSINIT is the common security initialization
						routine for the BSM or an ESM. BSSINIT initializes and starts
						partition FB as BSM server partition unless another partition than FB
						has been specified in the IPL SYS command (SYS SERVPART=xx).</p>
					<p>To avoid any problems and additional customization work, it is
						recommended <b>not</b> to switch the predefined partition (FB) in
						which the <keyword conref="fa299ent.dita#common-fa299sym/ses"></keyword> runs after
						installation. If there is a need for a switch, you should plan it very
						carefully. You must select a static partition which is not controlled
						by <keyword conref="fa299ent.dita#common-fa299sym/power"></keyword> and modify the <keyword conref="fa299ent.dita#common-fa299sym/power"></keyword> startup procedure
						accordingly. The partition used must have the same priority as the
						default partition FB.</p>
					<p>Refer also to <xref href="fa2pl_issuing_bsm_security_server_cmds.dita#sscm"></xref>.</p></li>
<li>RSL Security available with the BSM <p>Using the BSM support that is
						available from <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> V3R1.1 onwards, you
						can use RSL (Resource Security Level) security to control access to <tm trademark="CICS" tmtype="reg">CICS</tm> resources such as: <ul compact="yes">
<li>Files</li>
<li>Programs</li>
<li>Transactions</li>
<li>Temporary storage</li>
<li>Transient data queues</li>
<li>Journals</li></ul></p></li>
<li>You can also use a program (BSTSAVER) to recreate the contents of your BSM
					control file using BSTADMIN commands. This is useful if you wish to create
					backup/safety copies of your BSM control file, or if you wish to migrate
					this file from one <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> release to a later one. For
					details, refer to <ph otherprops="toPDF" rev="JF">the topic <q>Migrating <tm trademark="CICS" tmtype="reg">CICS</tm> Transaction Security
							Definitions</q> in the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/admnx"></keyword></ph><xref href="fa2ad_migrating_cics_transaction_security_defs.dita" otherprops="toKC" rev="JF"></xref>.</li>
<li otherprops="future">A sequence of FSUs from a release/modification level of <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> that is earlier than V3R1.1
					will establish transaction security that is based upon the use of table
					DTSECTXN. You are responsible for migrating table DTSECTXN to the BSM
					control file, which is described in <ph otherprops="toPDF" rev="JF">the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/admnx"></keyword></ph><xref href="fa2ad_xref_target_for_toc.dita" format="dita" otherprops="toKC" rev="JF"></xref>.</li>
<li>Problem Solving in a Security Environment <p>In case of problem recovery,
						for example during the implementation and test phase, there may be a
						need to run your system without the security support active. If such a
						situation occurs, you can specify SEC=RECOVER in the IPL SYS command.
						Note, however, that this switches off security completely (including
						sign-on and <tm trademark="CICS" tmtype="reg">CICS</tm> transaction security) <i>leaving your
							system unprotected!</i></p></li></ul></p></conbody><?tm 1447078483 3?></concept>