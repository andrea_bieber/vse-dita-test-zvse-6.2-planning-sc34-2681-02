<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="osafeats" xml:lang="en-us">
<title>OSA-<tm trademark="Express" tmtype="reg">Express</tm> Features</title>
<prolog>
<metadata>
<keywords><indexterm>OSA-Express4s, OSA-Express3, OSA-Express2, OSA-Express</indexterm><indexterm>OSA-Express<indexterm>Gigabit Ethernet</indexterm></indexterm><indexterm>Gigabit Ethernet</indexterm><indexterm>OSA-Express<indexterm>Fast Ethernet</indexterm></indexterm><indexterm>Fast Ethernet</indexterm><indexterm>OSA-Express<indexterm>ATM</indexterm></indexterm><indexterm>ATM</indexterm><indexterm>OSA-Express<indexterm>Token Ring </indexterm></indexterm><indexterm>Token Ring</indexterm><indexterm>OSA-Express<indexterm>1000BASE-T Ethernet</indexterm></indexterm><indexterm>1000BASE-T Ethernet</indexterm><indexterm>OSA-Express<indexterm>Intra-Ensemble Data Network
					(IEDN)</indexterm></indexterm><indexterm>Intra-Ensemble Data Network (IEDN)</indexterm><indexterm>OSA-Express2 10 Gigabit Ethernet (10 GbE) support</indexterm><indexterm>10 Gigabit Ethernet (10 GbE) support (OSA-Express2)</indexterm><indexterm>OSA-Express2 Gigabit Ethernet (GbE) support</indexterm><indexterm>OSA-Express4s support</indexterm><indexterm>OSA-Express5s support</indexterm><indexterm>Gigabit Ethernet (GbE) support (OSA-Express2)</indexterm><indexterm>adapter-interruption facility for OSA-Express</indexterm><indexterm>QDIO adapter-interruption facility</indexterm></keywords></metadata></prolog>
<conbody>
<p>OSA-<tm trademark="Express" tmtype="reg">Express</tm> offers various features, as shown in
<xref href="#osafeats/tos6sfet"></xref>, <xref href="#osafeats/tos5sfet"></xref>, <xref href="#osafeats/tos4sfet" id="Tos4sfet-1313"></xref>, <xref href="#osafeats/tos3feat"></xref>, and <xref href="#osafeats/tos2feat"></xref>.</p>
<note type="other" othertype="Notes">
<ol>
<li>Starting with OSA-Express3, the GbE and 1000BASE-T Ethernet features are
					available with four ports and two CHPIDs. This means, two ports (Port-0 and
					Port-1) per CHPID.</li></ol>
		</note>
<p>	<table frame="all" pgwide="0" rowheader="firstcol" id="tos6sfet" rev="z14">
<title>OSA-Express6S Features Available With <keyword conref="fa299ent.dita#common-fa299sym/ibmz"></keyword></title>
<tgroup cols="5"><colspec colname="col1" colwidth="1046*" align="left"></colspec><colspec colname="colspec6" colwidth="671*"></colspec><colspec colname="colspec5" colwidth="671*"></colspec><colspec colname="COLSPEC4" colwidth="671*"></colspec><colspec colname="colspec7" colwidth="671*"></colspec>
<thead>
<row valign="bottom">
<entry colname="col1">Feature</entry>
<entry colname="colspec6">z196 / z114</entry>
<entry colname="colspec5">zEC12 / zBC12</entry>
<entry colname="COLSPEC4">z13 / z13s</entry>
<entry colname="colspec7">z14</entry></row></thead>
<tbody>
<row>
<entry colname="col1">1000BASE-T 2–ports per CHPID</entry>
<entry colname="colspec6">No</entry>
<entry colname="colspec5">No</entry>
<entry colname="COLSPEC4">No</entry>
<entry colname="colspec7">Yes</entry></row>
<row>
<entry colname="col1">Gigabit Ethernet (LX and SX) 2–ports per CHPID</entry>
<entry colname="colspec6">No</entry>
<entry colname="colspec5">No</entry>
<entry colname="COLSPEC4">No</entry>
<entry colname="colspec7">Yes</entry></row>
<row>
<entry colname="col1">10 Gigabit Ethernet (LR and SR) 1–port per CHPID</entry>
<entry colname="colspec6">No</entry>
<entry colname="colspec5">No</entry>
<entry colname="COLSPEC4">No</entry>
<entry colname="colspec7">Yes</entry></row></tbody></tgroup></table></p>
<p>			<table frame="all" pgwide="0" rowheader="firstcol" id="tos5sfet" rev="z14">
<title>OSA-Express5S Features Available With <keyword conref="fa299ent.dita#common-fa299sym/ibmz"></keyword></title>
<tgroup cols="5"><colspec colname="col1" colwidth="1046*" align="left"></colspec><colspec colname="colspec6" colwidth="671*"></colspec><colspec colname="colspec5" colwidth="671*"></colspec><colspec colname="COLSPEC4" colwidth="671*"></colspec><colspec colname="colspec7" colwidth="671*"></colspec>
<thead>
<row valign="bottom">
<entry colname="col1">Feature</entry>
<entry colname="colspec6">z196 / z114</entry>
<entry colname="colspec5">zEC12 / zBC12</entry>
<entry colname="COLSPEC4">z13 / z13s</entry>
<entry colname="colspec7">z14</entry></row></thead>
<tbody>
<row>
<entry colname="col1">1000BASE-T 2–ports per CHPID</entry>
<entry colname="colspec6">No</entry>
<entry colname="colspec5">Yes</entry>
<entry colname="COLSPEC4">Yes</entry>
<entry colname="colspec7">Yes</entry></row>
<row>
<entry colname="col1">Gigabit Ethernet (LX and SX) 2–ports per CHPID</entry>
<entry colname="colspec6">No</entry>
<entry colname="colspec5">Yes</entry>
<entry colname="COLSPEC4">Yes</entry>
<entry colname="colspec7">Yes</entry></row>
<row>
<entry colname="col1">10 Gigabit Ethernet (LR and SR) 1–port per CHPID</entry>
<entry colname="colspec6">No</entry>
<entry colname="colspec5">Yes</entry>
<entry colname="COLSPEC4">Yes</entry>
<entry colname="colspec7">Yes</entry></row></tbody></tgroup></table></p>
<p>			<table frame="all" pgwide="0" rowheader="firstcol" id="tos4sfet" rev="r62">
<title>OSA-Express4S Features Available With <keyword conref="fa299ent.dita#common-fa299sym/ibmz"></keyword></title>
<tgroup cols="5"><colspec colname="col1" colwidth="1046*" align="left"></colspec><colspec colname="colspec6" colwidth="671*"></colspec><colspec colname="colspec5" colwidth="671*"></colspec><colspec colname="COLSPEC7" colwidth="671*"></colspec><colspec colname="colspec8" colwidth="671*"></colspec>
<thead>
<row valign="bottom">
<entry colname="col1">Feature</entry>
<entry colname="colspec6">z196 / z114</entry>
<entry colname="colspec5">zEC12 / zBC12</entry>
<entry colname="COLSPEC7">z13 / z13s</entry>
<entry colname="colspec8">z14</entry></row></thead>
<tbody>
<row>
<entry colname="col1">1000BASE-T 2–ports per CHPID</entry>
<entry colname="colspec6">Yes</entry>
<entry colname="colspec5">Yes</entry>
<entry colname="COLSPEC7">Yes</entry>
<entry colname="colspec8">Yes</entry></row>
<row>
<entry colname="col1">Gigabit Ethernet (LX and SX) 2–ports per CHPID</entry>
<entry colname="colspec6">Yes</entry>
<entry colname="colspec5">Yes</entry>
<entry colname="COLSPEC7">Yes</entry>
<entry colname="colspec8">No</entry></row>
<row>
<entry colname="col1">10 Gigabit Ethernet (LR and SR) 1–port per CHPID</entry>
<entry colname="colspec6">Yes</entry>
<entry colname="colspec5">Yes</entry>
<entry colname="COLSPEC7">Yes</entry>
<entry colname="colspec8">No</entry></row></tbody></tgroup></table></p>
<p>			<table frame="all" pgwide="0" rowheader="firstcol" id="tos3feat" rev="z14">
<title>OSA-Express3 Features Available With <keyword conref="fa299ent.dita#common-fa299sym/ibmz"></keyword></title>
<tgroup cols="4"><colspec colname="col1" colwidth="1046*" align="left"></colspec><colspec colname="colspec6" colwidth="671*"></colspec><colspec colname="colspec5" colwidth="671*"></colspec><colspec colname="COLSPEC8" colwidth="671*"></colspec>
<thead>
<row valign="bottom">
<entry colname="col1">Feature</entry>
<entry colname="colspec6">z196 / z114</entry>
<entry colname="colspec5">zEC12 / zBC12</entry>
<entry colname="COLSPEC8">z13 / z13s / z14</entry></row></thead>
<tbody>
<row>
<entry colname="col1">Gigabit Ethernet (LX and SX) 4–ports and 2 CHPIDs</entry>
<entry colname="colspec6">Yes</entry>
<entry colname="colspec5">Yes</entry>
<entry colname="COLSPEC8">No</entry></row>
<row>
<entry colname="col1">10 Gigabit Ethernet LR 2–ports and 2 CHPIDs</entry>
<entry colname="colspec6">Yes</entry>
<entry colname="colspec5">Yes</entry>
<entry colname="COLSPEC8">No</entry></row>
<row>
<entry colname="col1">1000BASE-T Ethernet 4-port and 2 CHPIDs</entry>
<entry colname="colspec6">Yes</entry>
<entry colname="colspec5">Yes</entry>
<entry colname="COLSPEC8">No</entry></row></tbody></tgroup></table>
		</p>
<p>			<table frame="all" pgwide="0" rowheader="firstcol" id="tos2feat" rev="z14">
<title>OSA-Express2 Features Available With <keyword conref="fa299ent.dita#common-fa299sym/ibmz"></keyword></title>
<tgroup cols="4"><colspec colname="col1" colwidth="1046*" align="left"></colspec><colspec colname="colspec7" colwidth="671*"></colspec><colspec colname="colspec4" colwidth="671*"></colspec><colspec colname="COLSPEC9" colwidth="671*"></colspec>
<thead>
<row valign="bottom">
<entry colname="col1">Feature</entry>
<entry colname="colspec7">z196 / z114</entry>
<entry colname="colspec4">zEC12 / zBC12</entry>
<entry colname="COLSPEC9">z13 / z13s / z14</entry></row></thead>
<tbody>
<row>
<entry colname="col1">Gigabit Ethernet (LX and SX)</entry>
<entry colname="colspec7">Yes</entry>
<entry colname="colspec4">No</entry>
<entry colname="COLSPEC9">No</entry></row>
<row>
<entry colname="col1">10 Gigabit Ethernet LR</entry>
<entry colname="colspec7">No</entry>
<entry colname="colspec4">No</entry>
<entry colname="COLSPEC9">No</entry></row>
<row>
<entry colname="col1">1000BASE-T Ethernet</entry>
<entry colname="colspec7">Yes</entry>
<entry colname="colspec4">No</entry>
<entry colname="COLSPEC9">No</entry></row></tbody></tgroup></table></p>
<p>You can configure your OSA-<tm trademark="Express" tmtype="reg">Express</tm> adapter with various CHPID types. The CHPID
			type determines how the OSA-<tm trademark="Express" tmtype="reg">Express</tm> adapter will be used.</p>
<p><xref href="#osafeats/tchptyp"></xref> describes the CHPID types you can configure. <table frame="all" pgwide="0" rowheader="firstcol" id="tchptyp" rev="z14">
<title>CHPID Types Supported by OSA-Express Features</title>
<tgroup cols="3"><colspec colname="col1" colwidth="386*" align="left"></colspec><colspec colname="colspec3" colwidth="927*" align="left"></colspec><colspec colname="colspec5" colwidth="1004*" align="left"></colspec>
<thead>
<row valign="bottom">
<entry colname="col1">CHPID Type</entry>
<entry colname="colspec3">OSA-Express6S, OSA-Express5S, OSA-Express4S, OSA-Express3,
OSA-Express2</entry>
<entry colname="colspec5">Where Used </entry></row></thead>
<tbody>
<row>
<entry colname="col1">OSC</entry>
<entry colname="colspec3"> 1000BASE-T Ethernet </entry>
<entry colname="colspec5">OSA-ICC (for emulation of TN3270E and non-SNA DFT 3270).</entry></row>
<row>
<entry colname="col1">OSD</entry>
<entry colname="colspec3"><lines>1000BASE-T Ethernet
Gigabit Ethernet
10 Gigabit Ethernet</lines></entry>
<entry colname="colspec5">Queue Direct Input/Output (QDIO) architecture: TCP/IP traffic when Layer 3
(uses IP address) and Protocol-independent when Layer 2 (uses MAC address).</entry></row>
<row>
<entry colname="col1">OSE</entry>
<entry colname="colspec3">1000BASE-T Ethernet</entry>
<entry colname="colspec5">The adapter executes in non-QDIO mode, and can be used for: <ul compact="yes">
<li>SNA connections</li>
<li>APPN connections</li>
<li>TCP/IP links</li></ul></entry></row>
<row>
<entry colname="col1">OSN</entry>
<entry colname="colspec3">1000BASE-T Ethernet</entry>
<entry colname="colspec5">OSA-Express for NCP: Appears to z/VSE as a device-supporting channel data
link control (CDLC) protocol. Enables Network Control Program (NCP) channel-related functions, such
as loading and dumping to NCP. Available on System z9® and up to z13. Not supported on GbE features
starting with OSA-Express4S. Not supported by z14 and later.</entry></row>
<row>
<entry colname="col1">OSX</entry>
<entry colname="colspec3">10 Gigabit Ethernet </entry>
<entry colname="colspec5">OSA-Express for zBX. Provides connectivity and access control to the
intraensemble data network (IEDN) from z196 and later to Unified Resource Manager functions.
Available on <b>z196 CEC</b> and later, and <b>OSA-Express3</b> and later. </entry></row></tbody></tgroup></table></p>
<p>The CHPID types shown in <xref href="#osafeats/tchptyp"></xref> are now explained in more detail in the topics that
			now follow.</p></conbody><?tm 1447078413 14?></concept>