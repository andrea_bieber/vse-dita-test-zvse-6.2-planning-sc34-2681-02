<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="signshut" xml:lang="en-us">
<title>Signal Quiesce (Signal Shutdown) Support</title>
<prolog>
<metadata>
<keywords><indexterm>signal shutdown support</indexterm><indexterm>signal quiesce support</indexterm><indexterm>signal quiesce event</indexterm></keywords></metadata></prolog>
<conbody>
<p>A <i>signal quiesce</i> (also referred to as <i>signal shutdown</i>) requests an
			end-of-program execution for a <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> system that has been enabled for
			signal quiesce.</p>
<p>To configure <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> for signal quiesce, you use the
			QUIESCE parameter of the IPL SYS control statement.</p>
<p>If a disruptive operation (for example, an IML or IPL) is performed via a <i>Service
				Element</i> (SE) or <i>Hardware Management Console</i> (HMC) panel, the SE or
			HMC will generate a <i>signal-quiesce event</i>. Under <keyword conref="fa299ent.dita#common-fa299sym/vme"></keyword>, a signal-quiesce event can be
			triggered for a guest using a SIGNAL SHUTDOWN command. The signal-quiesce event
			instructs an active <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> to quiesce (stop) processing: <ol>
<li>When a signal-quiesce event is received, <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> issues message 0W01D:
					<codeblock>0W01D DO YOU WANT TO CONTINUE SYSTEM SHUTDOWN (WILL BE FORCED AFTER TIMEOUT)?
REPLY 'YES' TO ENTER HARD WAIT STATE OR 'NO'</codeblock></li>
<li>If the operator replies <q>YES</q>, the system will enter a disabled-wait
					state and set a unique hard-wait code. <p>If the operator replies <q>NO</q>
						or does not reply at all, the system will wait for a predefined
						time-interval only, after which it continues to process the disruptive
						operation.</p></li></ol></p>
<p>Signal quiesce is designed to be mainly used with <i>console automation</i> programs
			which can initiate a <i>controlled system shutdown</i> as a response to message
			0W01D. <note><keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> does <b>not</b> provide
				controlled shutdown processing!</note></p>
<p>			<ul>
<li>For further details of the IPL SYS QUIESCE parameter, refer to <ph otherprops="toPDF" rev="JF"><q>Initial Program Load</q> in the <xref keyref="scsz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/scs"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/scsx"></keyword></ph><xref href="fa2sc_r_ipl.dita" otherprops="toKC" rev="JF"></xref>.</li>
<li>If you run <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> under <keyword conref="fa299ent.dita#common-fa299sym/vme"></keyword>, also refer to the <cite><tm trademark="z/VM" tmtype="reg">z/VM</tm> CP Commands and Utilities
						Reference</cite>. This publication describes the SIGNAL SHUTDOWN
					command and its impact on <keyword conref="fa299ent.dita#common-fa299sym/vme"></keyword> Guest operating
					systems.</li></ul></p></conbody><?tm 1447078411 1?></concept>