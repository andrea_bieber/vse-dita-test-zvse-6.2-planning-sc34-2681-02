<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="dvme" xml:lang="en-us">
<title>Defining a VM Directory Entry</title>
<prolog>
<metadata>
<keywords><indexterm>defining<indexterm>VM directory entry for <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> guest</indexterm></indexterm><indexterm>VM directory</indexterm><indexterm>VM<indexterm>directory
entry for <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> guest</indexterm></indexterm><indexterm><keyword conref="fa299ent.dita#common-fa299sym/v"></keyword><indexterm>directory
entry under VM</indexterm></indexterm><indexterm>directory entry for <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> guest</indexterm><indexterm>minidisks (z/VM)</indexterm><indexterm>CKD minidisks (z/VM)</indexterm><indexterm>FBA minidisks (z/VM)</indexterm></keywords></metadata></prolog>
<conbody>
<p>Each guest system running under VM requires a <i>directory entry</i> that
defines the virtual machine's configuration and operating characteristics.
For the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> system
volumes DOSRES and SYSWK1, VM requires unique volume IDs if they are
shared by two or more <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> guest
systems running under VM.</p>
<p><xref href="fa2pl_define_vm_directory_entry.dita#dvme/fegvmdir"></xref> shows
a sample VM directory entry for a <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> system named <i>VSEESA1</i>.
The letters to the left of the figure are used here as reference points
for various statements.  They are not part of the actual directory
entry.</p>
<note>When you install <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> under
VM, <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> will
automatically sense the devices defined in the VM directory for its
configuration. If you have any devices not supported under VM, you
can bypass device sensing by adding the EML operand in the IPL ADD
statements of <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>.</note>
<fig expanse="column" id="fegvmdir">
<title>Example of a VM Directory</title>
<codeblock>(A)  USER VSEESA1 password 1024M 8G G
(B)  OPTION MAINTCCW QUICKDSP
(C)  MACHINE ESA 2
(D)  CPU 0 CPUID 0xxxxx NODEDICATE
     CPU 1 CPUID 1xxxxx
(E)  IPL 24A0 PARM AUTOLOG
(F)  ACCOUNT ### SYSPROG
(G)    DEDICATE 244 704
       DEDICATE 245 705
(H)    CONSOLE 009 3270 T OPERATOR
(I)    SPECIAL 080 3270
              .
              .
       SPECIAL 01F 3270
(J)    SPOOL   00C 3505 A
       SPOOL   00D 3525 A
       SPOOL   00E 3262 A
       SPOOL   05E 4248 A
              .
              .

            * Link to optional disks as needed....
            * Link to Executable CMS Code

(K)    LINK MAINT 190 190 RR

            * Link to Program Products (Y Disk)

       LINK MAINT 19E 19E RR
(L)    MDISK 240 3390 1 1112 VSADOS MWV
       MDISK 241 3390 1 1112 VSASY1 MWV
       MDISK 242 3390 1 1112 USER01 MWV
       MDISK 243 3390 1 1112 USER02 MWV</codeblock></fig>
<p id="dir">The statements in the directory define the following:
 <dl>
<dlentry>
<dt>A</dt>
<dd><b>USER</b> defines the:  <ul>
<li>Name and password of the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> guest.</li>
<li>Virtual storage size (1024M or 1G) for the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> guest when logging
on to VM (this is the real storage for <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> itself).</li>
<li>Maximum virtual storage (8G) size that can be defined for the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> guest after logging
on. This is therefore the maximum size for VM.</li>
<li>User class G (general) is available.</li></ul></dd></dlentry>
<dlentry>
<dt>B</dt>
<dd><b>OPTION</b> defines:  <ul>
<li>MAINTCCW authorizes the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> guest to initialize
disk devices (use diagnostic CCWs).</li>
<li>QUICKDSP causes a virtual machine to be added to the dispatch
list immediately when it has work to do, without waiting in the eligible
list.</li></ul></dd></dlentry>
<dlentry>
<dt>C</dt>
<dd><b>MACHINE</b> defines the virtual machine mode (which must be
ESA). <b>2</b> defines the maximum number of processors that can be
active.</dd></dlentry>
<dlentry>
<dt>D</dt>
<dd><b>CPU</b> defines the processors that can be active.</dd></dlentry>
<dlentry>
<dt>E</dt>
<dd><b>IPL</b> defines automatic IPL to be performed with specified
device address.</dd></dlentry>
<dlentry>
<dt>F</dt>
<dd><b>ACCOUNT</b> defines an account number and a distribution identification.</dd></dlentry>
<dlentry>
<dt>G</dt>
<dd><b>DEDICATE</b> specifies that a real disk device is to be dedicated
to this <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> guest
(VSEESA1).</dd></dlentry>
<dlentry>
<dt>H</dt>
<dd><b>CONSOLE</b> defines the virtual machine console. In the statement:
 <ul>
<li>009 is the virtual address of the console.</li>
<li>3270 defines the terminal type of the virtual machine console.</li>
<li>T defines the spool class.</li>
<li>OPERATOR defines the secondary VM user.  If the primary user (VSEESA1)
is disconnected, the VM user OPERATOR will receive all CP messages
for the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> virtual
machine. The secondary user also can send CP commands to the disconnected <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> virtual machine.</li></ul></dd></dlentry>
<dlentry>
<dt>I</dt>
<dd><b>SPECIAL</b> defines a virtual device with device type and address
to the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> system.
Terminal addresses do <i>not</i> have to be real devices on the system.
 <p>When you define a terminal as SPECIAL, you can use the address
to dial into the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> system.
 You should ensure that you have a sufficient number of addresses
defined as SPECIAL for users who require the DIAL function.</p>  <p>Devices
defined in this manner are added to the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> IPL procedure
through device sensing.</p></dd></dlentry>
<dlentry>
<dt>J</dt>
<dd><b>SPOOL</b> defines virtual unit record devices. One entry is
required for each unit record device.</dd></dlentry>
<dlentry>
<dt>K</dt>
<dd><b>LINK</b> defines a link to a device that belongs to another
user.</dd></dlentry>
<dlentry>
<dt>L</dt>
<dd><b>MDISK</b> defines an extent on a disk device to be owned by
VSEESA1.  The extent assigned with the statement becomes the user's
minidisk.  <p>In case of FBA minidisks, the total number of blocks
must be a multiple of the number of blocks per access position for
the device type. <q>Multiple of access position</q> means cylinder
boundary and its values should be obtained from the related device
documentation.</p> <p>For further LINK and MDISK information, refer
to <xref href="fa2pl_define_shared_minidisks.dita#minis"></xref>.</p></dd></dlentry></dl></p></conbody><?tm 1447078452 0?></concept>