<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="ipl359n" xml:lang="en-us">
<title>IPL and JCL Support</title>
<prolog>
<metadata>
<keywords><!--Insert &lt;indexterm&gt; tags here--></keywords></metadata></prolog>
<conbody>
<p>The <tm trademark="IBM" tmtype="reg">IBM</tm> 3590/3592 will be properly recognized during IPL, if it
			is attached and operational. If the device is not attached and operational, the <tm trademark="IBM" tmtype="reg">IBM</tm> 3590/3592 can be added by specifying a tape device type
			in the type of ADD statement: <codeblock>ADD  cuu,TPA[,mode]</codeblock>In this
			second example, the <i>tape device type</i> is simply <codeph>TPA</codeph>:
			<codeblock>ADD 580,TPA,2B </codeblock>In this third example, the <i>tape device
				type</i> is <codeph>TPA896</codeph> which contains more precise information
			about the tape device and its track format (<codeph>TPA896</codeph> corresponds to an <tm trademark="IBM" tmtype="reg">IBM</tm> 3592 E05 with encryption mode):
			<codeblock>ADD 580,TPA896,2B</codeblock></p>
<note>From <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> 4.3 onwards, you <i>cannot</i> ADD a
			TPA tape drive with tape mode '05' or with a tape mode that includes '05' in its
			calculation (such as '25'). <ph otherprops="future">If you have specified one of these tape modes in a previous <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> release, during an initial
				installation or FSU of <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> you will receive an appropriate
				error message.</ph></note>
<p>JCL allows you to assign <tm trademark="IBM" tmtype="reg">IBM</tm> 3590/3592 tape drives using one of these methods: <ul>
<li>By specifying the cuu directly: <codeblock>ASSGN SYSxxx,cuu</codeblock></li>
<li>By specifying the tape device type TPA with which the device (cuu) was added
					during IPL: <codeblock>ASSGN SYSxxx,TPAnnn</codeblock></li></ul> where <codeph>TPAnnn</codeph> can be TPA, TPAT128, TPAT264, and so on for the
			different tape device types and their corresponding track formats.</p>
<p>JCL also allows LIBSERV MOUNT commands for <tm trademark="IBM" tmtype="reg">IBM</tm> 3590/3592 tape drive units in tape libraries (such as
			the <tm trademark="IBM" tmtype="reg">IBM</tm> 3494). If several tape-device types are contained in a
			tape library and a UNIT parameter has not been specified, JCL will select a tape
			drive that is able to process the specified tape volumes.</p>
<p>Data compaction is handled as specified during IPL in the ADD statement, or as set with
			the JCL ASSGN statement. Volume, header and trailer labels are not compacted.</p>
<p>For details of how to specify tape device types, refer to <ph otherprops="toPDF" rev="JF"><q>Job Control and Attention Routine</q> in the <xref keyref="scsz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/scs"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/scsx"></keyword></ph><xref href="fa2sc_r_jobcontrol_ar.dita" otherprops="toKC" rev="JF"></xref>.</p></conbody><?tm 1447078419 6?></concept>