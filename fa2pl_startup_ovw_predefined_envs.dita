<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="plg22" xml:lang="en-us">
<title>Startup Overview of Predefined Environments</title>
<prolog>
<metadata>
<keywords><indexterm>predefined<indexterm>ENVIRONMENTS, STARTUP OVERVIEW</indexterm></indexterm><indexterm>startup<indexterm>overview</indexterm></indexterm></keywords></metadata></prolog>
<conbody>
<p>The following table provides an overview for the predefined environments
about page data set extent books, partition allocation and startup,
and the JCL startup procedures, jobs, and skeletons associated with
it.</p>
<p>  <table pgwide="0" rowheader="firstcol" id="envirs">
<title>Startup
Overview of <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> Predefined
Environments</title>
<desc>
<p outputclass="summary"><!--Table summary--></p></desc>
<tgroup cols="4"><colspec colname="col1" colwidth="104*"></colspec><colspec colname="col2" colwidth="92*"></colspec><colspec colname="col3" colwidth="87*"></colspec><colspec colname="colspec2" colwidth="93*"></colspec>
<thead>
<row>
<entry colname="col1" valign="top"></entry>
<entry colname="col2" align="center" valign="top">ENVIRONMENT A</entry>
<entry colname="col3" align="center" valign="top">ENVIRONMENT B</entry>
<entry colname="colspec2" align="center" valign="top">ENVIRONMENT
C</entry></row></thead>
<tbody>
<row>
<entry colname="col1"> <codeblock>Partitions 
(see Note 1)</codeblock></entry>
<entry colname="col2" align="left"> <codeblock>12 Static
28 Dynamic</codeblock></entry>
<entry colname="col3" align="left"> <codeblock>12 Static
48 Dynamic</codeblock></entry>
<entry colname="colspec2"> <codeblock>12 Static
108 Dynamic</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>Address Spaces</codeblock></entry>
<entry colname="col2" align="left"> <codeblock>12/48</codeblock></entry>
<entry colname="col3" align="left"> <codeblock>12/68</codeblock></entry>
<entry colname="colspec2"> <codeblock>12/108  </codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>VSIZE (in MB) and 
related Page Data Set 
Extent Books 
(see Note 2)</codeblock></entry>
<entry colname="col2" align="left"> <codeblock>256
 
PDSxxxx0/2</codeblock></entry>
<entry colname="col3" align="left"> <codeblock>512
 
PDSxxxx0/2</codeblock></entry>
<entry colname="colspec2"> <codeblock>2048
 
PDSxxxx0/2</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>Partition Allocation
Procedure:
Skeletons:
(see Note 3)</codeblock></entry>
<entry colname="col2"> <codeblock> 
ALLOC
SKALLOCA
 </codeblock></entry>
<entry colname="col3"> <codeblock> 
ALLOC
SKALLOCB
 </codeblock></entry>
<entry colname="colspec2"> <codeblock> 
ALLOC
SKALLOCC
 </codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>BG Startup</codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Procedures:  $0JCL, USERBG
Skeletons:   SKJCL0, SKUSERBG</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>F1 Startup 
(VSE/POWER)</codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Procedures:  $1JCL
Skeletons:   SKJCL1</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>VSE/POWER Autostart
Procedures:
Skeletons:</codeblock></entry>
<entry colname="col2"> <codeblock> 
POWSTRTA
SKPWSTRT</codeblock></entry>
<entry colname="col3"> <codeblock> 
POWSTRTB
SKPWSTRT</codeblock></entry>
<entry colname="colspec2"> <codeblock> 
POWSTRTC
SKPWSTRT</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>F2 Startup 
(CICS TS, ICCF)</codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Procedure:   $2JCL
Startup Job: CICSICCF
Skeletons:   SKJCL2, SKCICS</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>F3 Startup 
(VTAM)</codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Procedure:   $3JCL
Startup Job: VTAMSTRT
Skeletons:   SKJCL3, SKVTAM</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>F4 Startup </codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Procedure:   $4JCL
Skeleton:   SKJCL4</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>F5 Startup</codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Procedure:   $5JCL
Skeleton:    SKJCL5</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>F6 - FA Startup</codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Procedure:   $6JCL - $AJCL
Skeletons:   SKJCL6 - SKJCLA</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>FB Startup 
(Security Server)</codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Procedure:   $BJCL
Skeleton:    SKJCLB</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>Dynamic Partition 
Startup</codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Profile:     STDPROF/PWSPROF
Skeleton:    SKJCLDYN</codeblock></entry></row>
<row>
<entry colname="col1"> <codeblock>Library Definitions</codeblock></entry>
<entry namest="col2" nameend="colspec2"> <codeblock>Procedure:   LIBDEF
Skeleton:    SKLIBCHN</codeblock></entry></row></tbody></tgroup></table> </p>
<note type="other" othertype="Notes">
<ol>
<li>The maximum number of partitions (static plus dynamic) that can
be active is defined with the NPARTS parameter of the IPL SYS command.
The current settings for NPARTS for each <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> predefined environment
are shown in <xref href="fa2pl_selecting_predefined_env.dita#plg11/tpasize"></xref>.</li>
<li>Listed are the names of the Z-books stored in IJSYSRS.SYSLIB.
Such a Z-book contains the DPD commands specifying the page data set
extents.  xxxx represents the disk device type (such as 3390) on which
the page data set extents reside.</li>
<li>During <keyword conref="conref-fa2plmm3.dita#conref-fa2plmm3/inin"></keyword>, <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> changes the procedure
name for partition allocation automatically to ALLOC. This procedure
is active once initial installation has been completed.</li></ol> </note></conbody><?tm 1447078433 0?></concept>