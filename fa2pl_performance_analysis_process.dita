<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="plg123" xml:lang="en-us">
<title>Performance Analysis Process</title>
<prolog>
<metadata>
<keywords><indexterm>performance considerations<indexterm>analysis
process</indexterm></indexterm></keywords></metadata></prolog>
<conbody>
<p>Whenever a performance problem is encountered, a structured and
logical analysis is important to solve the problem as fast as possible.
To this end, you should follow the outline provided on the following
pages and ask the following questions:  <ul>
<li>Where in the system does the problem occur?</li>
<li>What has been changed (if the problem did not occur before)?</li>
<li>When and with which application does the problem occur?</li>
<li>What actions do I need to take for I/O related problems?</li>
<li>Sizing a new IBM Z server for use with z/VSE.</li></ul></p>
<p rev="r62">In order to analyze a performance problem you will need to have meaningful performance
measurement data available. When you perform a hardware or software migration, it is strongly
recommended to keep a good set of performance measurement data taken on the original system before
the migration. The performance measurement data should include at least CPU utilization measurements
as well as I/O count and time measurements from a couple of weeks of a typical production workload.
The measurement interval should be 5 minutes or less to have a reasonable granularity of the data.
You should measure all related systems, all z/VM Guests (when running under z/VM), and all LPARs to
have a complete picture of all related systems. </p>
<p rev="r62">Having the measurement data allows you to compare the performance data from before and
after the migration. It is a good idea to keep the performance measurement data for an extended
period of time when performing a migration, as performance problems may even be realized only after
a longer time after the migration has been completed. </p>
<p rev="r62">Different performance monitoring tools are available, either from IBM or from
independent software vendors. The following list contains several performance monitoring tools,
however there might be additional tools provided by independent software vendors:<ul>
<li>z/VM Performance Toolkit (when z/VSE runs under z/VM)</li>
<li>Built-in commands: SIR, QUERY TD, SIR SMF, SIR MON, Job Accounting, MAP, GETVIS</li>
<li>CICS provided tools: CICS Statistics, CEMT INQUIRE</li>
<li>System Activity Dialogs of the z/VSE Interactive Interface</li>
<li>CPUMON Tool</li>
<li>Explore from, CA Technologies, Inc.</li>
<li>TMON from Allen Systems Group, Inc.</li>
<li>zVPS from Velocity Software (when z/VSE runs under z/VM)</li></ul></p>
<p><b><i>Where in the system does the problem occur?</i></b></p>
<p> <ol>
<li>For hardware and software related problems:  <sl>
<sli>Are the performance related ECs (Engineering Changes) and PTFs
(Program Temporary Fixes) installed?</sli></sl></li>
<li>For microcode and software setup:  <sl>
<sli>Are the VM assists active?</sli>
<sli>Are the VM guest parameters properly set?</sli>
<sli>Are all traces switched off?</sli>
<sli>Is DEBUG off?</sli></sl></li>
<li>For problems related to the system configuration:  <sl>
<sli>What is the CPU utilization?</sli>
<sli>What is the paging rate?</sli></sl></li>
<li>For I/O related problems:  <sl>
<sli>Use performance monitoring tools for a detailed problem analysis.</sli>
<sli>Is minidisk caching active?</sli></sl></li></ol> </p>
<p><b><i>What has been changed (if the problem did not occur before)?</i></b></p>
<p> <ul>
<li>Was there any change in the system, workload, or partition setup?</li>
<li>Has any new software release from <tm trademark="IBM" tmtype="reg">IBM</tm> or
from another supplier been installed?  <ul>
<li>Which new release was installed?</li>
<li>Do PTFs for this release exist?</li>
<li>Can I temporarily deactivate the software or even remove it?</li>
<li>Can I reproduce the problem?</li></ul></li></ul> </p>
<p><b><i>When and with which application does the problem occur?</i></b></p>
<p> <ul>
<li>Already in single thread?</li>
<li>Only under heavy system load?</li>
<li>Only when specific other applications are running?</li>
<li>Which particular job step or transaction is affected?</li>
<li>Did the problem exist before? If not, what has been changed?</li></ul> </p>
<p><b><i>What actions do I need to take for I/O related problems?</i></b></p>
<p> <ul>
<li>Did the number of I/Os increase compared to the former situation?</li>
<li>Did the duration of the I/Os increase?</li>
<li>Check the capacity guidelines for device and channel utilizations
and the number of actuators and paths.  <p>Consider that <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> supports dynamic
I/O handling, which includes dynamic path select (DPS) and dynamic
path reconnect (DPR).</p></li>
<li>Tune the software with regard to I/O as follows: <ul rev="r62">
<li>Check the <keyword conref="fa299ent.dita#common-fa299sym/vsam"></keyword> file definitions. <p>Use more
or larger I/O buffers, especially for a <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> system
with more private space.</p></li>
<li>Are other disk device types used without re-checking the following? <p>The <keyword conref="fa299ent.dita#common-fa299sym/vsam"></keyword> CI (control interval) sizes for data and index. The
layout of the <keyword conref="fa299ent.dita#common-fa299sym/vsam"></keyword> LSR buffers.</p></li></ul></li>
<li>Check your minidisk caching settings, and adjust if required.</li>
<li>Increase the multiprogramming level (I/O concurrency). This can
be done by using additional partitions.</li></ul> </p>
<p rev="r62"><b><i>Sizing a new IBM Z server for use with z/VSE</i></b></p>
<p rev="r62">When planning for a IBM Z server upgrade, it is essential to perform proper system
sizing and capacity planning. Meaningful performance measurement data is required to perform
capacity planning and sizing of new systems. </p>
<p rev="r62">IBM and business partners provide a free of charge capacity planning study which is
based on the zCP3000 tool. When running under z/VM, it requires measurement data collected by the
z/VM Performance Toolkit. When running in LPAR image (without z/VM), it requires measurement data
collected by the CPUMON Tool. </p>
<p rev="r62">Contact your business partner or your sales representative and ask for z/VSE Capacity
Planning support. </p></conbody><?tm 1447078493 9?></concept>