<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="plg134" xml:lang="en-us">
<title>Recommendations for Using DASD Sharing and Lock Files</title>
<prolog>
<metadata>
<keywords><indexterm>performance considerations<indexterm>DASD sharing
and lock file hints</indexterm></indexterm><indexterm>DASD sharing<indexterm>lock
file hints</indexterm></indexterm><indexterm>lock file hints</indexterm><indexterm>DASD sharing<indexterm>under performance aspects</indexterm></indexterm></keywords></metadata></prolog>
<conbody>
<p>When you share disk devices (DASD sharing), you can reduce the
overhead as far as possible by adhering to the following rules:  <ul>
<li>Put non-shared data on non-shared disk devices.  <p>This is a
general rule which does not only bring performance benefits, but also
is reasonable for non-performance reasons.</p></li>
<li>Avoid, whenever possible, to add disk devices as cuu,SHR.</li>
<li>Only specify as many CPUs as needed in the DLF NCPU parameter,
in order to speed up lock file I/Os.  <p>Increasing the total size
of the lock file (NCYLS) to 2 or more cylinders may reduce the I/Os
by increased <q>hashing hits</q>.</p></li>
<li>Lock File location:  <p>Put the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> lock file, if
possible, on a separate volume (in LPAR image). Under VM, use a VM
virtual disk, or at least a separate VM minidisk for the lock file.
Place only other data (if at all) on the lock file if it is rarely
accessed. If possible, select a string that is not highly utilized.
The reason is that:  <ul>
<li>RESERVE/RELEASE is being used by the VSE lock manager.</li>
<li>RESERVE/RELEASE locks a complete volume.</li></ul></p></li>
<li>Lock File volume:  <p>If all <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> systems sharing
the same lock file run under the same VM systems, you can use one
of the following as lock file volume:  <ul>
<li>A VM virtual disk</li>
<li>A separate VM minidisk</li></ul></p> <p>Instead of using the above and when running on different servers, you may use as lock file
volume one of the following: <ul rev="r62">
<li>A cached  IBM 3390</li></ul></p></li>
<li>Placing the lock file on a SCSI FCP-attached FBA Disk:  <p>If
you want to allocate a lock file on a SCSI disk, you must install
a unique FCP adapter for each VSE system that shares the lock file.
Each VSE system must access the lock file via its unique FCP adapter.
This is necessary because the hardware does not reserve the SCSI disk
(using RESERVE command) per FCP cuu, but instead per FCP adapter. <note type="other" othertype="Warning!">If the VSE systems sharing the lock
file also share the same FCP adapter to access the SCSI disk, the
lock file and the data to be protected may be destroyed.</note></p> <note type="other" othertype="Notes">
<ol>
<li>You cannot define the lock file on an FCP SCSI-attached DOSRES
or SYSWK1 disk, or on another IPL device. The IPL checks for the IPL
device.</li>
<li>You should not establish a multipath connection to an FCP SCSI-attached
device on which the lock file resides. If more than one connection
to this device exists,  the IPL will reject the DLF command for this
SCSI device. If a DEF/SYSDEF command for the lock file device is entered
after the DLF command, the FCP device driver will reject this command.</li>
<li>If the <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> system
enters hardwait, a release of the lock file is attempted in order
to prevent other VSE systems being affected that share the lock file.</li>
<li>During an IPL, the SCSI FCP device where the lock file resides
is released unconditionally.</li>
<li>If you use a VM virtual disk, you should ensure that the size
of your lock disk (including the VTOC) is a multiple of (777 x 8)
blocks.</li></ol> </note></li></ul></p></conbody><?tm 1447078496 2?></concept>