<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="sixf" xml:lang="en-us">
<title>FlashCopy Support</title>
<prolog>
<metadata>
<keywords><indexterm>$IJBIXFP, phasename of FlashCopy support</indexterm><indexterm>FlashCopy support, DS8000, DS6000, and ESS servers</indexterm><indexterm>FlashCopy Consistency Group support, for DS8000 series</indexterm><indexterm>FlashCopy SE, of DS8000 series</indexterm></keywords></metadata></prolog>
<conbody>
<p>IBM System Storage FlashCopy technology is a <i>point-in-time</i> copy capability that can be
used to help reduce planned application outages caused by backups and other data copy activities. It
is available with the DS8000, DS6000, and ESS servers.</p>
<p>FlashCopy is designed to enable data to be copied in the background, then make both source and
copied data available to users almost immediately. </p>
<p><keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> supports the following selected FlashCopy
functions: <ul>
<li>The NOCOPY option, which leads to greater efficiency when copying from disk to tape. The NOCOPY
function can be used to copy most, or all, of the data directly from the source to tape
<i>without</i> the need to first copy all of the physical data to a intermediate target copy.</li>
<li><i>Dataset Copy</i>, which offers a new level of granularity. It allows more efficient use of
disk capacity. It can also reduce background completion times because FlashCopy no longer needs to
be performed at the volume level when only a dataset copy is required. With Dataset Copy, the source
and target volumes may be different sizes. It also allows the copied data to reside at a different
location on the source and target volumes. <note>Because a VSAM <q>dataset</q> is a complex entity
consisting of a catalog entry and one or more data extents which can reside on different volumes,
VSAM files are <b>not</b> candidates for the Dataset Copy function.</note></li>
<li><i>Elimination of the LSS Constraint</i>, which can help simplify administration and capacity
planning for <tm trademark="FlashCopy" tmtype="reg">FlashCopy</tm>. Source and target volumes can
now span logical subsystems within a storage server.</li>
<li><i>Multiple Relationship FlashCopy</i>, which offers new flexibility. It allows up to 12 target
volumes to be created from one source volume in a single FlashCopy operation. Multiple target volumes can be used for testing, backup, and other
applications. </li>
<li rev="r62"><i>IBM FlashCopy SE</i> feature is a hardware feature of the <i><tm trademark="DS8000" tmtype="reg">DS8000</tm> series</i>. It allocates storage space on an <q>as needed</q> basis
by only using space on a target volume when it actually copies tracks from the source volume to the
target volume. For a source volume without much write activity during the duration of the
FlashCopy SE relation, the target volume might therefore consume significantly less physical
space than the source volume. </li>
<li><i>FlashCopy Consistency Group</i> support, which is
available with the ESS, DS6000, and DS8000 storage subsystems. It can be used when applications have
their data spread over multiple volumes. <ul>
<li>This support uses the IXFP SNAP command with parameter FREEZE, followed by IXFP DDSR
command with parameter THAW.</li>
<li>ICKDSF supports the PPRC FREEZE and PPRC RUN commands. Note that these commands have the same
functionality as the IXFP SNAP command with the FREEZE parameter, and the IXFP DDSR command with the
THAW parameter.</li></ul></li></ul></p>
<p rev="r62">For example, <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> does <b>not</b>
support the following FlashCopy functions: <ul compact="yes">
<li>Incremental <tm trademark="FlashCopy" tmtype="reg">FlashCopy</tm>.</li>
<li>PersistentFlashCopy Relationship.</li>
<li>Inband Commands over a Remote Mirror link.</li></ul></p>
<p>For details of how to: <ul>
<li>use FlashCopy support, including the <i>IBM FlashCopy SE</i> and <i>Consistency Group
<tm trademark="FlashCopy" tmtype="reg">FlashCopy</tm></i> features, refer to <ph otherprops="toPDF" rev="JF"><q>Performing a FlashCopy</q> in the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/admnx"></keyword></ph><xref href="fa2ad_performing_flashcopy.dita" otherprops="toKC" rev="JF"></xref>.</li>
<li>take a snapshot of <i>VSE/VSAM files</i>, refer to <q>Performing an IDCAMS
						SNAP (<tm trademark="FlashCopy" tmtype="reg">FlashCopy</tm>)</q> in the <xref keyref="vsprz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/vspr"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/vsprx"></keyword>.</li></ul></p></conbody><?tm 1447078415 28?></concept>