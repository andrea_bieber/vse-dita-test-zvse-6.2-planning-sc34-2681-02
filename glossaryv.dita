<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE dita PUBLIC "-//IBM//DTD DITA IBM Composite//EN"
 "ibm-ditabase.dtd">
<dita>
<topic id="glossaryv" xml:lang="en-us">
<title>V</title>
<glossentry id="fa2_g_variable_length_rel_rec_data_set" xml:lang="en-us">
<glossterm>variable-length relative-record data set (VRDS)</glossterm><glossdef>A relative-record data set with variable-length records.
See also <i>relative-record data set</i>.</glossdef></glossentry>
<glossentry id="fa2_g_variable_length_rel_rec_file" xml:lang="en-us">
<glossterm>variable-length relative-record file</glossterm><glossdef>A VSE/VSAM relative-record file with variable-length records.
See also <i>relative-record file</i>.</glossdef></glossentry>
<glossentry id="fa2_g_vio" xml:lang="en-us">
<glossterm>VIO</glossterm><glossdef>See virtual I/O area.</glossdef></glossentry>
<glossentry id="fa2_g_virtual_address" xml:lang="en-us">
<glossterm>virtual address</glossterm><glossdef>An address that refers to a location in virtual storage.
It is translated by the system to a processor storage address when
the information stored at the virtual address is to be used.</glossdef></glossentry>
<glossentry id="fa2_g_virtual_addressability_extension" xml:lang="en-us">
<glossterm>virtual addressability extension (VAE)</glossterm><glossdef>A storage management support that allows to use multiple
virtual address spaces.</glossdef></glossentry>
<glossentry id="fa2_g_virtual_address_space" xml:lang="en-us">
<glossterm>virtual address space</glossterm><glossdef>A subdivision of the virtual address area (virtual storage)
available to the user for the allocation of private, nonshared partitions.</glossdef></glossentry>
<glossentry id="fa2_g_virtual_disk" xml:lang="en-us">
<glossterm>virtual disk</glossterm><glossdef>A range of up to 2 gigabytes of contiguous virtual storage
addresses that a program can use as workspace. Although the virtual
disk exists in storage, it appears as a real FBA disk device to the
user program. All I/O operations that are directed to a virtual disk
are intercepted and the data to be written to, or read from, the disk
is moved to or from a data space.
<p>Like a data space, a virtual disk
can hold only user data; it does not contain shared areas, system
data, or programs. Unlike an address space or a data space, data is
not directly addressable on a virtual disk. To manipulate data on
a virtual disk, the program must perform I/O operations.</p>
<p>Starting
with z/VSE 5.2, a virtual disk may be defined in a shared memory object.</p></glossdef></glossentry>
<glossentry id="fa2_g_virtual_io_area" xml:lang="en-us">
<glossterm>virtual I/O area (VIO)</glossterm><glossdef>An extension of the page data set; used by the system as
intermediate storage, primarily for control data.</glossdef></glossentry>
<glossentry id="fa2_g_virtual_mode" xml:lang="en-us">
<glossterm>virtual mode</glossterm><glossdef>The operating mode of a program, where the virtual storage
of the program can be paged, if not enough processor (real) storage
is available to back the virtual storage.</glossdef></glossentry>
<glossentry id="fa2_g_virtual_partition" xml:lang="en-us">
<glossterm>virtual partition</glossterm><glossdef>In VSE, a division of the dynamic area of virtual storage.</glossdef></glossentry>
<glossentry id="fa2_g_virtual_storage" xml:lang="en-us">
<glossterm>virtual storage</glossterm><glossdef>Addressable space image for the user from which instructions
and data are mapped into processor storage locations.</glossdef></glossentry>
<glossentry id="fa2_g_virtual_tape" xml:lang="en-us">
<glossterm>virtual tape</glossterm><glossdef>In z/VSE, a virtual tape is a file (or data set) containing
a tape image. You can read from or write to a virtual tape in the
same way as if it were a physical tape. A virtual tape can be: 
<ul compact="yes">
<li>A VSE/VSAM ESDS file on the z/VSE local system.</li>
<li>A remote file on the server side; for example, a Linux, UNIX,
or Windows file. To access such a remote virtual tape, a TCP/IP connection
is required between z/VSE and the remote system.</li></ul></glossdef></glossentry>
<glossentry id="fa2_g_volume_id" xml:lang="en-us">
<glossterm>volume ID</glossterm><glossdef>The volume serial number, which is a number in a volume
label that is assigned when a volume is prepared for use by the system.</glossdef></glossentry>
<glossentry id="fa2_g_vrds" xml:lang="en-us">
<glossterm>VRDS</glossterm><glossdef>Variable-length relative-record data sets. See <i>variable-length
relative record file</i>.</glossdef></glossentry>
<glossentry id="fa2_g_vsam" xml:lang="en-us">
<glossterm>VSAM</glossterm><glossdef>See <i>VSE/VSAM</i>.</glossdef></glossentry>
<glossentry id="fa2_g_vse" xml:lang="en-us">
<glossterm>VSE (Virtual Storage Extended)</glossterm><glossdef>A system that consists of a basic operating system and any
IBM supplied and user-written programs that are required to meet the
data processing needs of a user. VSE and hardware it controls form
a complete computing system. Its current version is called z/VSE.</glossdef></glossentry>
<glossentry id="fa2_g_vse_adv_functions" xml:lang="en-us">
<glossterm>VSE/Advanced Functions</glossterm><glossdef>A program that provides basic system control and includes
the supervisor and system programs such as the Librarian and the Linkage
Editor.</glossdef></glossentry>
<glossentry id="fa2_g_vse_connector_server" xml:lang="en-us">
<glossterm>VSE Connector Server</glossterm><glossdef>Is the host part of the VSE JavaBeans, and is started using
the job STARTVCS, which is placed in the reader queue during installation
of z/VSE. Runs by default in dynamic class R.</glossdef></glossentry>
<glossentry id="fa2_g_vse_ditto" xml:lang="en-us">
<glossterm>VSE/DITTO (VSE/Data Interfile Transfer, Testing, and Operations
Utility)</glossterm><glossdef>An IBM licensed program that provides file-to-file services
for disk, tape, and card devices.</glossdef></glossentry>
<glossentry id="fa2_g_vse_esa" xml:lang="en-us">
<glossterm>VSE/ESA (Virtual Storage Extended/Enterprise Systems Architecture)</glossterm><glossdef>The predecessor system of z/VSE.</glossdef></glossentry>
<glossentry id="fa2_g_vse_fast_copy" xml:lang="en-us">
<glossterm>VSE/Fast Copy</glossterm><glossdef>A utility program for fast copy data operations from disk
to disk and dump/restore operations via an intermediate dump file
on magnetic tape or disk.</glossdef></glossentry>
<glossentry id="fa2_g_vse_fcopy" xml:lang="en-us">
<glossterm>VSE/FCOPY (VSE/Fast Copy Data Set program)</glossterm><glossdef>An IBM licensed program for fast copy data operations from
disk to disk and dump/restore operations via an intermediate dump
file on magnetic tape or disk. There is also a stand-alone version:
the FASTCOPY utility.</glossdef></glossentry>
<glossentry id="fa2_g_vse_iccf" xml:lang="en-us">
<glossterm>VSE/ICCF (VSE/Interactive Computing and Control Facility)</glossterm><glossdef>An IBM licensed program that serves as interface, on a time-slice
basis, to authorized users of terminals that are linked to the system's
processor.</glossdef></glossentry>
<glossentry id="fa2_g_vse_iccf_lib" xml:lang="en-us">
<glossterm>VSE/ICCF library</glossterm><glossdef>A file that is composed of smaller files (libraries) including
system and user data, which can be accessed under the control of VSE/ICCF.</glossdef></glossentry>
<glossentry id="fa2_g_vse_java_beans" xml:lang="en-us">
<glossterm>VSE JavaBeans</glossterm><glossdef>Are JavaBeans that allow access to all VSE-based file systems
(VSE/VSAM, Librarian, and VSE/ICCF), submit jobs, and access the z/VSE
operator console. The class library is contained in the <i>VSEConnector.jar</i> archive.
See also <i>JavaBeans</i>.</glossdef></glossentry>
<glossentry id="fa2_g_vse_library" xml:lang="en-us">
<glossterm>VSE library</glossterm><glossdef>A collection of programs in various forms and storage dumps
stored on disk. The form of a program is indicated by its member type
such as source code, object module, phase, or procedure. A VSE library
consists of at least one sublibrary, which can contain any type of
member.</glossdef></glossentry>
<glossentry id="fa2_g_vse_power" xml:lang="en-us">
<glossterm>VSE/POWER</glossterm><glossdef>An IBM licensed program that is primarily used to spool
input and output. The program's networking functions enable a VSE
system to exchange files with or run jobs on another remote processor.</glossdef></glossentry>
<glossentry id="fa2_g_vse_vsam" xml:lang="en-us">
<glossterm>VSE/VSAM (VSE/Virtual Storage Access Method)</glossterm><glossdef>An IBM access method for direct or sequential processing
of fixed and variable length records on disk devices.</glossdef></glossentry>
<glossentry id="fa2_g_vse_vsam_catalog" xml:lang="en-us">
<glossterm>VSE/VSAM catalog</glossterm><glossdef>A file containing extensive file and volume information
that VSE/VSAM requires to locate files, to allocate and deallocate
storage space, to verify the authorization of a program or an operator
to gain access to a file, and to accumulate use statistics for files.</glossdef></glossentry>
<glossentry id="fa2_g_vse_vsam_managed_space" xml:lang="en-us">
<glossterm>VSE/VSAM managed space</glossterm><glossdef>A user-defined space on disk that is placed under the control
of VSE/VSAM.</glossdef></glossentry></topic></dita>