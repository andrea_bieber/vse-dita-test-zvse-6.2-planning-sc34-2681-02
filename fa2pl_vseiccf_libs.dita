<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="iclib" xml:lang="en-us">
<title><keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword> Libraries</title>
<prolog>
<metadata>
<keywords><indexterm>VSE/ICCF<indexterm>library contents</indexterm></indexterm><indexterm>VSE/ICCF<indexterm>DTSFILE</indexterm></indexterm><indexterm>libraries<indexterm>VSE/ICCF</indexterm></indexterm><indexterm>DTSFILE</indexterm><indexterm>libraries<indexterm>VSE user libraries</indexterm></indexterm><indexterm>planning for<indexterm>VSE user libraries</indexterm></indexterm><indexterm>user<indexterm>libraries</indexterm></indexterm><indexterm>defining<indexterm>VSE user libraries</indexterm></indexterm><indexterm>creating<indexterm>VSE user libraries</indexterm></indexterm><indexterm>dialogs for<indexterm>creating VSE user
					libraries</indexterm></indexterm></keywords></metadata></prolog>
<conbody>
<p>The <keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword> DTSFILE is <ph>allocated with
				approximately 40MB and defines 199 <keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword> libraries and users during
				initial installation.</ph> These libraries are also referred to as <i>program
				development</i> libraries.</p>
<p><xref href="#iclib/iccflib"></xref> shows the <keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword> libraries and their use. Note that
			some libraries are reserved for <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>. The members that <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> ships in these libraries take up
			approximately 20% of the space reserved for the DTSFILE. After allocating a library
			for each user, determine the total requirement for the DTSFILE. Compare this to the
			default allocation. If the space is insufficient, you should extend the file.</p>
<p>For information on how to extend the DTSFILE, refer to <ph otherprops="toPDF" rev="JF">the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/admnx"></keyword> under <q>Using Skeleton
					SKDTSEXT</q></ph><xref href="fa2ad_use_skeleton_skdtsext.dita" otherprops="toKC" rev="JF"></xref>. It describes how to use the skeleton <i>SKDTSEXT</i> for this task. </p>
<table pgwide="0" rowheader="firstcol" id="iccflib">
<title><keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword> Libraries</title>
<tgroup cols="4" colsep="1" rowsep="1"><colspec colname="col1" colwidth="56*"></colspec><colspec colname="col2" colwidth="70*"></colspec><colspec colname="col3" colwidth="207*"></colspec><colspec colname="col4" colwidth="53*"></colspec>
<thead>
<row>
<entry rowsep="1" valign="top">Library</entry>
<entry rowsep="1" valign="top">Type</entry>
<entry rowsep="1" valign="top">Contents</entry>
<entry rowsep="1" valign="top">Usage</entry></row></thead>
<tbody>
<row>
<entry valign="top">1</entry>
<entry valign="top">Private</entry>
<entry valign="top">VSE/ICCF administrative library. Contents shipped
							with VSE/ICCF.</entry>
<entry valign="top">system</entry></row>
<row>
<entry valign="top">2</entry>
<entry valign="top">Common</entry>
<entry valign="top">Common library. Macros and procedures. VSE/ICCF and <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> code
							members.</entry>
<entry valign="top">system</entry></row>
<row>
<entry valign="top">3 - 6</entry>
<entry valign="top">Public</entry>
<entry valign="top">Empty.</entry>
<entry valign="top">user</entry></row>
<row>
<entry valign="top">7</entry>
<entry valign="top">Private</entry>
<entry valign="top">Empty.</entry>
<entry valign="top">user</entry></row>
<row>
<entry valign="top">8</entry>
<entry valign="top">Private</entry>
<entry valign="top">Default primary library for operator
							profile.</entry>
<entry valign="top">user</entry></row>
<row>
<entry valign="top">9</entry>
<entry valign="top">Private</entry>
<entry valign="top">Default primary library for programmer
							profile.</entry>
<entry valign="top">user</entry></row>
<row>
<entry valign="top">10</entry>
<entry valign="top">Private</entry>
<entry valign="top">Default primary library for administrator
							profile.</entry>
<entry valign="top">user</entry></row>
<row>
<entry valign="top">11 - 49</entry>
<entry valign="top">Private</entry>
<entry valign="top">Empty.</entry>
<entry valign="top">user</entry></row>
<row>
<entry valign="top">50 - 58</entry>
<entry valign="top">Public</entry>
<entry valign="top">Reserved for <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>.</entry>
<entry valign="top">system</entry></row>
<row>
<entry valign="top">59</entry>
<entry valign="top">Public</entry>
<entry valign="top"><keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> job streams,
							skeletons, <keyword conref="fa299ent.dita#common-fa299sym/cicss"></keyword> tables, and
							sample programs for the workstation file transfer
							support.</entry>
<entry valign="top">system</entry></row>
<row>
<entry valign="top">60 - 67</entry>
<entry valign="top">Public</entry>
<entry valign="top">Reserved for <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>.</entry>
<entry valign="top">system</entry></row>
<row>
<entry valign="top">68</entry>
<entry valign="top">Public</entry>
<entry valign="top"><keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> members for
							Personal Computer tasks.</entry>
<entry valign="top">system</entry></row>
<row>
<entry valign="top">69</entry>
<entry valign="top">Public</entry>
<entry valign="top">Reserved for <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword>.</entry>
<entry valign="top">system</entry></row>
<row>
<entry valign="top">70 - 199</entry>
<entry valign="top">Private</entry>
<entry valign="top">Empty.</entry>
<entry valign="top">user</entry></row></tbody></tgroup></table>
<p>For information about changing the characteristics of the DTSFILE, refer to <ph otherprops="toPDF" rev="JF">the topic <q>Regenerating the Supervisor, <keyword conref="fa299ent.dita#common-fa299sym/power"></keyword>, or <keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword></q> in the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/admnx"></keyword></ph><xref href="fa2ad_regenerating_supervisor_vsepower_or_vseiccf.dita" otherprops="toKC" rev="JF"></xref>.</p>
<note>The <i>Program Development Library</i> dialog helps you access and use <keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword> libraries. For detailed
			information about this dialog, refer to the <xref keyref="euserz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/euser"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/euserx"></keyword> under <q>Handling VSE/ICCF
				Library Members</q>. </note></conbody><?tm 1447078449 0?></concept>