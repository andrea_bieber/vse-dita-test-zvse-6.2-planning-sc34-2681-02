<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="plg80" xml:lang="en-us">
<title>Procedure CPUVARn</title>
<prolog>
<metadata>
<keywords><indexterm>system startup<indexterm>CPUVARn procedure</indexterm></indexterm><indexterm>system startup<indexterm>SETPARM statements</indexterm></indexterm><indexterm>partitions, system<indexterm>status at
					startup</indexterm></indexterm><indexterm>CPUVARn procedure</indexterm><indexterm>status of partitions at startup</indexterm><indexterm>SETPARM statements for system variables</indexterm><indexterm>system startup<indexterm>DTRSETP utility
					program</indexterm></indexterm><indexterm>DTRSETP utility program</indexterm><indexterm>synchronization points used for startup</indexterm></keywords></metadata></prolog>
<conbody>
<p>A CPUVARn procedure consists of SETPARM statements that contain system variables. These
			variables describe the system and reflect each partition's status. For a each CPU,
			one procedure is required. <b>n</b> is the CPU number. The name of the default
			procedure shipped is CPUVAR1.</p>
<p>A CPUVARn procedure is used to save system information such as: <ul>
<li><b>Use of Partitions</b> – This information shows which partitions are
					used and which programs run in the partitions.</li>
<li><b>Partition Status</b> – A partition can be either active or
					inactive. For example, if a partition shows status ACTIVE at startup time,
					it indicates that the previous shutdown was not successful.</li>
<li><b>Outstanding Requests</b> – <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> functions may request a
					certain partition startup mode. For example, a COLD start is requested for
					the <keyword conref="fa299ent.dita#common-fa299sym/power"></keyword> partition after the <keyword conref="fa299ent.dita#common-fa299sym/power"></keyword> queues have been
					extended.</li>
<li><b>Results of Status Analysis and Operator Requests</b> – The startup
					program DTRISTRT updates system variables according to these requests.</li>
<li><b>Synchronization Points</b> – Synchronization points can be used to
					define how partitions interact during startup. For example, a
					synchronization point in one partition may trigger further processing in
					another partition.</li>
<li><b>System Environment</b> – This is the environment that was selected
					at <keyword conref="conref-fa2plmm3.dita#conref-fa2plmm3/inin"></keyword>.</li></ul></p>
<note>As described in <ph otherprops="toPDF" rev="JF">the <xref keyref="utilz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/util"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/utilx"></keyword> under <q>DTRSETP
				Utility</q></ph><xref href="fa2ut_part3_dtrsetp_dtrsetp.dita" otherprops="toKC" rev="JF"></xref>, the <i>DTRSETP</i> utility program helps you create and modify
			CPUVARn procedures.</note></conbody></concept>