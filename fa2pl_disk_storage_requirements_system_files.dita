<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="dksp" xml:lang="en-us">
<title>Disk Storage Requirements for System Files</title>
<prolog>
<metadata>
<keywords><indexterm>disk storage, second <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword></indexterm></keywords></metadata></prolog>
<conbody>
<p>While some system files are shared between the two <tm trademark="CICS" tmtype="reg">CICS</tm> Transaction Servers, others must be defined
			additionally for the second <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword>.</p>
<p>The following files are shared with DBDCCICS, the primary <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword>:
			<codeblock>   CICS.CSD
   CICS.DBDCCICS.DFHDMFA
   CICS.DBDCCICS.DFHDMFB
   PC.HOST.TRANSFER.FILE
   VSE.CONTROL.FILE
   VSE.TEXT.REPSTORY.FILE
   VSE.MESSAGE.ROUTING.FILE</codeblock>
			The system files to be defined for the second <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword> are shown in <xref href="#dksp/scidsp"></xref>. Initially, you should reserve the amount of space
			shown.</p>
<p><keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> provides skeleton <b>SKPREPC2</b> to
			define these system files. <indexterm>CICS Transaction Server<indexterm>files for
					second <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword></indexterm></indexterm></p>
<table frame="all" pgwide="0" rowheader="firstcol" id="scidsp">
<title>System Files and Disk Space Needed for Second Predefined <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword></title>
<desc>
<p outputclass="summary"><!--Table summary--></p></desc>
<tgroup cols="5"><colspec colname="col1" colwidth="15*"></colspec><colspec colname="col2" colwidth="36*"></colspec><colspec colname="col5" colwidth="14*"></colspec><colspec colname="colspec0" colwidth="14*"></colspec><colspec colname="col6" colwidth="14*"></colspec>
<thead>
<row valign="bottom">
<entry colname="col1">File Name</entry>
<entry colname="col2">IBM-Provided File Identifier </entry>
<entry namest="col5" nameend="colspec0">File Type</entry>
<entry colname="col6">Remarks</entry></row></thead>
<tbody>
<row>
<entry colname="col1">DFHRSD</entry>
<entry colname="col2">CICS2.RSD</entry>
<entry namest="col5" nameend="colspec0">VSAM KSDS</entry>
<entry colname="col6">See Note 1</entry></row>
<row>
<entry colname="col1">DFHNTRA</entry>
<entry colname="col2">CICS2.TD.INTRA</entry>
<entry namest="col5" nameend="colspec0">VSAM ESDS</entry>
<entry colname="col6">See Note 1</entry></row>
<row>
<entry colname="col1">DFHTEMP</entry>
<entry colname="col2">CICS2.DFHTEMP</entry>
<entry namest="col5" nameend="colspec0">VSAM ESDS</entry>
<entry colname="col6">See Note 1</entry></row>
<row>
<entry colname="col1">DFHGCD</entry>
<entry colname="col2">CICS2.GCD</entry>
<entry namest="col5" nameend="colspec0">VSAM KSDS</entry>
<entry colname="col6"></entry></row>
<row>
<entry colname="col1">DFHLCD</entry>
<entry colname="col2">CICS2.LCD</entry>
<entry namest="col5" nameend="colspec0">VSAM KSDS</entry>
<entry colname="col6"></entry></row>
<row>
<entry colname="col1"></entry>
<entry colname="col2"></entry>
<entry namest="col5" nameend="colspec0"></entry>
<entry colname="col6"></entry></row>
<row>
<entry colname="col1">DFHDMPA</entry>
<entry colname="col2">CICS2.DUMPA</entry>
<entry namest="col5" nameend="colspec0">VSAM ESDS</entry>
<entry colname="col6"></entry></row>
<row>
<entry colname="col1">DFHDMPB</entry>
<entry colname="col2">CICS2.DUMPB</entry>
<entry namest="col5" nameend="colspec0">VSAM ESDS</entry>
<entry colname="col6"></entry></row>
<row>
<entry colname="col1">DFHAUXT</entry>
<entry colname="col2">CICS2.AUXTRACE</entry>
<entry namest="col5" nameend="colspec0">VSAM ESDS</entry>
<entry colname="col6"></entry></row>
<row>
<entry colname="col1">IESPRB</entry>
<entry colname="col2">CICS2.ONLINE.PROB.DET.FILE</entry>
<entry namest="col5" nameend="colspec0">VSAM ESDS</entry>
<entry colname="col6"></entry></row>
<row>
<entry namest="col1" nameend="col6">Approximate Amount of Disk Space Required</entry></row>
<row>
<entry colname="col1" morerows="1"></entry>
<entry colname="col2" morerows="1">Number of FBA Blocks</entry>
<entry namest="col5" nameend="colspec0">Number of Tracks (is a multiple of cylinders)</entry>
<entry colname="col6" morerows="1">Approx. Number of Megabytes (MB)</entry></row>
<row>
<entry colname="col5" namest="col5" nameend="colspec0">IBM 3390</entry></row>
<row>
<entry colname="col1">VSAM Space </entry>
<entry colname="col2">24,000</entry>
<entry colname="col5" namest="col5" nameend="colspec0">390</entry>
<entry colname="col6">12</entry></row>
<row>
<entry colname="col1">Journal Files (see Note 2)</entry>
<entry colname="col2">12,032</entry>
<entry colname="col5" namest="col5" nameend="colspec0">240</entry>
<entry colname="col6">6</entry></row></tbody></tgroup></table>
<p>All files are ready for use after skeleton SKPREPC2 has been run. <note type="other" othertype="Notes">
<ol>
<li>The file is created dynamically when required. It is released again (on
						user request) when it is no longer needed.</li>
<li><ph>Possibly, your second <keyword conref="fa299ent.dita#common-fa299sym/cicsts"></keyword> needs system
							journal files (DFHJCTxx). The size of a system journal file</ph>
						and of any <tm trademark="CICS" tmtype="reg">CICS</tm> user journal file that you might want
						to use, is workload dependent. For guidance about journal file sizes,
						refer to the appropriate <keyword conref="fa299ent.dita#common-fa299sym/cics"></keyword> documentation. Add
						the size of the planned journal files to your disk space requirements.
							<p>For <tm trademark="CICS" tmtype="reg">CICS</tm> journal files, refer also to
							"Note on <tm trademark="CICS" tmtype="reg">CICS</tm> Journaling" under <xref href="fa2pl_non_vsevsam_system_files.dita#nvsf"></xref>.</p></li></ol>
			</note></p></conbody><?tm 1447078473 6?></concept>