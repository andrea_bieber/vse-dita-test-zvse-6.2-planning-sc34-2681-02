<?xml version="1.0" encoding="UTF-8"?>
<!--Arbortext, Inc., 1988-2011, v.4002-->
<!DOCTYPE concept PUBLIC "-//IBM//DTD DITA IBM Concept//EN"
 "ibm-concept.dtd">
<concept id="signldap" xml:lang="en-us">
<title><keyword conref="fa299ent.dita#common-fa299sym/ldapsignu"></keyword> Support</title>
<prolog>
<metadata>
<keywords><indexterm>LDAP sign-on security</indexterm><indexterm>security support<indexterm>LDAP sign-on
					security</indexterm></indexterm></keywords></metadata></prolog>
<conbody>
<p>The <keyword conref="fa299ent.dita#common-fa299sym/ldapsignl"></keyword> support enables users to sign
			on to <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> using long, <q>company-wide</q>
			(corporate) user-IDs and passwords. The user-ID and password are authenticated using
			an LDAP server that is reachable via the TCP/IP network. The LDAP server is installed
			on the company network.</p>
<p>This use of <q>company-wide</q> user-IDs connects <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> with the <i>centralized</i>
			management of user-IDs. As a result, password-rules and password-renewal can be
			enforced via the LDAP server. </p>
<p>LDAP authorization is designed to integrate <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> into <q>Identity Management
				Systems</q>, such as <tm trademark="IBM" tmtype="reg">IBM</tm>
			<tm trademark="Tivoli" tmtype="reg">Tivoli</tm> products, or systems-based on the <i><tm trademark="Microsoft" tmtype="tm">Microsoft</tm> Active Directory</i> that support LDAP
			authentication.</p>
<p>An <keyword conref="fa299ent.dita#common-fa299sym/ldapsignl"></keyword> overcomes the previous
			limitations in <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> that: <ul>
<li><keyword conref="fa299ent.dita#common-fa299sym/iccf"></keyword> user-IDs could be up to
					4 characters long.</li>
<li><tm trademark="CICS" tmtype="reg">CICS</tm> user-IDs could be between 4 and 8
					characters long.</li>
<li>Passwords could be up to 8 characters long.</li></ul></p>
<p>Using an <keyword conref="fa299ent.dita#common-fa299sym/ldapsignl"></keyword>: <ul>
<li>User-IDs and passwords can <i>both</i> be up to 64 characters long.</li>
<li>Users can be forced to use <i>complex</i> passwords. For example, passwords
					might have to contain a mixture of numerical and alphabetical characters,
					and/or upper and lower case characters.</li>
<li>A <i>centralized</i> management of <q>company-wide</q> user-IDs and
					passwords within a company is possible. Each user can be forced to change
					his/her password that is used on <i>all</i> systems within a predefined
					time period (for example, every 12 weeks).</li></ul></p>
<p>The major advantage of a centralized <i>Identity Management System</i> is that all
			updates for employees (enabling/disabling passwords, granting/revoking access, and so
			on) can be performed on <i>one</i> location. For example, if an employee leaves a
			company, the <i>company's administrator</i> must only revoke access for the person's
			user-IDs in the Identity Management System (typically, the LDAP server). This means,
			that the revoked user-IDs can no longer be used for signing on to <keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> without the need for the <i><keyword conref="fa299ent.dita#common-fa299sym/v"></keyword> administrator</i> to perform any
			further actions.</p>
<p>For further information, refer to <ph otherprops="toPDF" rev="JF"><q>Maintaining User Profiles in an LDAP Environment</q> in the <xref keyref="admnz" scope="external"><i><keyword conref="fa299ent.dita#common-fa299sym/admn"></keyword></i></xref>, <keyword conref="fa299ent.dita#common-fa299sym/admnx"></keyword></ph><xref href="fa2ad_maintain_user_profiles_in_ldap_env.dita" otherprops="toKC" rev="JF"></xref>. General information about LDAP and Identity Management Systems can
			also be found on the internet using any standard Search Engine.</p></conbody><?tm 1447078485 4?></concept>